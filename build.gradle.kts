import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.6.10"
    application
}

group = "com.gitlab.linde9821"
version = "0.1.0"

repositories {
    mavenCentral()
}

dependencies {

    val processingVersion = "3.3.7"

    implementation("org.processing:core:$processingVersion")
    // required if you need to use the pdf renderer
    implementation("org.processing:pdf:$processingVersion")

    // if you need more libraries from the processing organisation take a look here https://mvnrepository.com/artifact/org.processing

    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClass.set("StrangeAttractorSampleKt")
}
