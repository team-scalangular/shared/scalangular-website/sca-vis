# Processing Kotlin Gradle Template

A project which can be used as a template for processing projects using kotlin. More information can
be found [here](https://discourse.processing.org/t/processing-3-kotlin-project-template-with-gradle/35378).

## Getting started

Just clone or fork this repo and use it as a basis for your project

## Known Issues

* The `P2D` and `P3D` renderers don't work on apple m1 chips:

This is because of an error in the macOS arm build of the used java OpenGL binding library (jogl)
which is used by Processing. (I think a fix is available [4] but the best solution would be a fix in
the processing library)
